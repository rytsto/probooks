<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $timestamps = false;
    
    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
