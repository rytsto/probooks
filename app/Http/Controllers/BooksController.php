<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\testdir\test;
use App\Book;
use App\User;
use App\ReadListBook;
use App\Comment;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class BooksController extends Controller
{
    public function getBookById($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $book = Book::find($id);
        if($book != null){
            return response()->json($book, 200);
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function getAllBooks(){
        $books = Book::all();
        $BooksResult = [
            'books' => $books,
        ];
        return response()->json($BooksResult, 200);
    }

    public function createBook(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'genre' => 'required|string|max:255',
            'pages' => 'required|integer|max:5000|min:1',
            'description' => 'required|string|max:2000',
            'release_date' => 'required|integer|max:2019',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        if($loggedInUserInfo->role != 1){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $bodyContent = $request->all();
            $book = new Book;
            $book->title = $bodyContent["title"];
            $book->author = $bodyContent["author"];
            $book->genre = $bodyContent["genre"];
            $book->pages = $bodyContent["pages"];
            $book->description = $bodyContent["description"];
            $book->release_date = $bodyContent["release_date"];
            $book->save();
            return response()->json($book, 201);
        }
    }

    public function editBook($id, Request $request){ 
        if(!is_numeric($id)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'genre' => 'required|string|max:255',
            'pages' => 'required|integer|max:5000|min:1',
            'description' => 'required|string|max:255',
            'release_date' => 'required|integer|max:2019',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        if($loggedInUserInfo->role != 1){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $book = Book::find($id);
            if(empty($book)){
                return response()->json(array("error"=>"Not Found"), 404);
            }
            else{
                $bodyContent = $request->all();
                $book->title = $bodyContent["title"];
                $book->author = $bodyContent["author"];
                $book->genre = $bodyContent["genre"];
                $book->pages = $bodyContent["pages"];
                $book->description = $bodyContent["description"];
                $book->release_date = $bodyContent["release_date"];
                $book->save();
                return response()->json($book, 200);
            }
        }
    }

    public function deleteBook($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);

        if($loggedInUserInfo->role != 1){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $book = Book::find($id);
            if(empty($book)){
                return response()->json(array("error"=>"Not Found"), 404);
            }
            else{
                ReadListBook::where('book_id', $id)->delete();
                Comment::where('book_id', $id)->delete();
                $book->delete();
                return response()->json(null, 204);
            }
        }
    }
}
