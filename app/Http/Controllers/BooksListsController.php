<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\testdir\test;
use App\ReadList;
use App\User;
use App\ReadListBook;
use App\Comment;
use App\Book;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class BooksListsController extends Controller
{
    public function getBooksListById($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $bookList = ReadList::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
       
        if($bookList != null){
            if($bookList->is_public == 1 || $bookList->user_id == $loggedInUser->id){
                $books = ReadListBook::where('read_list_id', $bookList->id)->get();
                return response()->json(compact('bookList', 'books'),200);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function getBooksListsByUser($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $user = User::find($id);
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
       
        if($user != null){
            if($user->id == $loggedInUser->id){
                $booklists = ReadList::where('user_id', $id)->get();
                $BookListsResult = [
                    'books lists' => $booklists,
                ];
                return response()->json($BookListsResult, 200);
            }
            else{
                $booklists = ReadList::where('user_id', $id)->where('is_public', 1)->get();
                $BookListsResult = [
                    'books lists' => $booklists,
                ];
                return response()->json($BookListsResult, 200);
            }
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function getPublicBooksLists(){
        $booklists = ReadList::where('is_public', 1)->get();
        $BookListsResult = [
            'books lists' => $booklists,
        ];
        return response()->json($BookListsResult, 200);
    }

    public function createBooksList(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'is_public' => 'required|integer|max:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
            $bodyContent = $request->all();
            $bookList = new ReadList;
            $bookList->title = $bodyContent["title"];
            $bookList->description = $bodyContent["description"];
            $bookList->user_id = $loggedInUser->id;
            $bookList->is_public = $bodyContent["is_public"];
            $bookList->save();
            return response()->json($bookList, 201);
        }
    }

    public function editBooksList($id, Request $request){
        if(!is_numeric($id)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'is_public' => 'required|integer|max:1',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $bodyContent = $request->all();
        $bookList = ReadList::find($id);
        if($bookList != null){ 
            if($loggedInUser->id == $bookList->user_id){
                $bookList->title = $bodyContent["title"];
                $bookList->description = $bodyContent["description"];
                $bookList->is_public = $bodyContent["is_public"];
                $bookList->save();
                return response()->json($bookList, 200);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function deleteBooksList($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $bookList = ReadList::find($id);
        if($bookList != null){
            if(!($loggedInUserInfo->role == 1 || $bookList->user_id == $loggedInUser->id)){
                return response()->json(array("error"=>"Forbidden"), 403);
            }
            else{
                ReadListBook::where('read_list_id', $id)->delete();
                Comment::where('read_list_id', $id)->delete();
                $bookList->delete();
                return response()->json(null, 204);
            }
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function addBookToList($id, Request $request){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $validator = Validator::make($request->all(), [
            'book_id' => 'required|integer',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $bookList = ReadList::find($id); 
        if($bookList != null)
        {
            if($bookList->user_id == $loggedInUser->id){
                $bodyContent = $request->all();
                $checkBookInList = ReadListBook::where('read_list_id', $bookList->id)->where('book_id', $bodyContent["book_id"])->get();
                if(count($checkBookInList) >= 1){
                    return response()->json(array("error"=>"Bad Request"), 400);
                }
                $bookInList = new ReadListBook;
                $bookInList->is_read = 0;
                $bookInList->book_id = $bodyContent["book_id"];
                $bookInList->read_list_id = $id;
                $bookInList->save();
                return response()->json($bookInList, 201);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function setBookState($listId, $bookInListId, Request $request){
        if(!is_numeric($listId) || !is_numeric($bookInListId)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $validator = Validator::make($request->all(), [
            'is_read' => 'required|integer',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $bookList = ReadList::find($listId);
        $book = Book::find($bookInListId);
        if(empty($book) || empty($bookList)){
            return response()->json(array("error"=>"Not Found"), 404);
        } 
        else{
            if($bookList->user_id == $loggedInUser->id){
                $bodyContent = $request->all();
                $bookInList = ReadListBook::where('read_list_id', $bookList->id)->where('book_id', $bookInListId)->first();
                if(empty($bookInList)){
                    return response()->json(array("error"=>"Bad Request"), 400);
                }
                $bookInList->is_read = $bodyContent["is_read"];
                $bookInList->save();
                return response()->json($bookInList, 200);
            }
            else{
                return response()->json(array("error"=>"Forbidden"), 403);
            }
        }
    }

    public function removeBookFromList($listId, $bookInListId){
        if(!is_numeric($listId) || !is_numeric($bookInListId)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 

        $bookList = ReadList::find($listId);
        $book = Book::find($bookInListId);
        if(empty($book) || empty($bookList)){
            return response()->json(array("error"=>"Not Found"), 404);
        } 
        else{
            if($loggedInUser->id != $bookList->user_id){
                return response()->json(array("error"=>"Forbidden"), 403);
            }
            $bookInList = ReadListBook::where('read_list_id', $bookList->id)->where('book_id', $bookInListId)->first();
            if(empty($bookInList)){
                return response()->json(array("error"=>"Bad Request"), 400);
            }
            $bookInList->delete();
            return response()->json(null, 204);
        }
    }
}
