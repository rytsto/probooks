<?php

namespace App\Http\Controllers;
use App\testdir\test;
use Illuminate\Http\Request;
use App\Book;
use App\ReadList;
use App\User;
use App\Comment;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class CommentsController extends Controller
{
    public function getCommentById($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $comment = Comment::find($id);
        if($comment != null){
            return response()->json($comment, 200);
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function getAllCommentsByList($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $comments = Comment::where('read_list_id', $id)->get();
        $list = ReadList::find($id);

        if($list == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }

        if($list->is_public == 1){
            $commentsResult = [
                'comments' => $comments,
            ];
            return response()->json($commentsResult, 200);
        }
        else{
            return response()->json(array("error"=>"Bad Request"), 400);
        }
    }

    public function getAllCommentsByBook($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $comments = Comment::where('book_id', $id)->get();
        $book = Book::find($id);

        if($book == null){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        else{
            $commentsResult = [
                'comments' => $comments,
            ];
            return response()->json($commentsResult, 200);
        }
    }

    public function createBookComment($id, Request $request){
        $validator = Validator::make($request->all(), [
            'message' => 'required|string|max:1000',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $book = Book::find($id);
        if(empty($book)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
            $bodyContent = $request->all();
            $comment = new Comment;
            $comment->message = $bodyContent["message"];
            $comment->user_id = $loggedInUser->id;
            $comment->book_id = $id;
            $comment->save();
            return response()->json($comment, 201);    
        }  
    }

    public function createListComment($id, Request $request){
        $validator = Validator::make($request->all(), [
            'message' => 'required|string|max:1000',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $list = ReadList::find($id);
        if(empty($list)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($list->is_public != 1){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        else{
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
                $bodyContent = $request->all();
                $comment = new Comment;
                $comment->message = $bodyContent["message"];
                $comment->user_id = $loggedInUser->id;
                $comment->read_list_id = $id;
                $comment->save();
                return response()->json($comment, 201);
        }  
    }

    public function editBookComment($bookId, $commentId, Request $request){
        $validator = Validator::make($request->all(), [
            'message' => 'required|string|max:1000',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        if(!is_numeric($bookId) || !is_numeric($commentId)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $book = Book::find($bookId);
        $comment = Comment::find($commentId);
        if(empty($book) || empty($comment)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($comment->book_id != $bookId) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        if($comment->user_id == $loggedInUser->id){
            $bodyContent = $request->all();
            $comment->message = $bodyContent["message"];  
            $comment->save();
            return response()->json($comment, 200);
        }
        else {
            return response()->json(array("error"=>"Unauthorized"), 401);
        }
    }

    public function editListComment($listId, $commentId, Request $request){
        $validator = Validator::make($request->all(), [
            'message' => 'required|string|max:1000',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        if(!is_numeric($listId) || !is_numeric($commentId)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $list = ReadList::find($listId);
        $comment = Comment::find($commentId);
        if(empty($list) || empty($comment)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($comment->read_list_id != $listId) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        if($comment->user_id == $loggedInUser->id && $list->is_public == 1){
            $bodyContent = $request->all();
            $comment->message = $bodyContent["message"];  
            $comment->save();
            return response()->json($comment, 200);
        }
        else {
            return response()->json(array("error"=>"Unauthorized"), 401);
        }
    }

    public function deleteBookComment($bookId, $commentId){
        if(!is_numeric($bookId) || !is_numeric($commentId)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $book = Book::find($bookId);
        $comment = Comment::find($commentId);

        if(empty($book) || empty($comment)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($comment->book_id != $bookId) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }

        if(!($comment->user_id == $loggedInUser->id || $loggedInUserInfo->role == 1)){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $comment->delete();
            return response()->json(null, 204);
        }
    }

    public function deleteListComment($listId, $commentId){
        if(!is_numeric($listId) || !is_numeric($commentId)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        $list = ReadList::find($listId);
        $comment = Comment::find($commentId);

        if(empty($list) || empty($comment)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($comment->read_list_id != $listId) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }

        if($list->is_public != 1){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        if(!($comment->user_id == $loggedInUser->id || $loggedInUserInfo->role == 1)){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $comment->delete();
            return response()->json(null, 204);
        }
    }
}
