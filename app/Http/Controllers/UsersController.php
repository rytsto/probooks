<?php

namespace App\Http\Controllers;
use App\testdir\test;
use Illuminate\Http\Request;
use App\User;
use App\ReadListBook;
use App\ReadList;
use App\Comment;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UsersController extends Controller
{
    public function getUserById($id){
        $user = User::find($id);
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        if($user != null){
            return response()->json($user, 200);
        }
        else{
            return response()->json(array("error"=>"Not Found"), 404);
        }
    }

    public function getAllUsers(Request $request){
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);
        if($loggedInUserInfo->role == 1){
            $users = User::all();
            $UsersResult = [
                'users' => $users,
            ];
            return response()->json($UsersResult, 200);
        }
       else{
            return response()->json(array("error"=>"Forbidden"), 403);
       }
    }

    public function setUserRole($id, Request $request){
        $validator = Validator::make($request->all(), [
        'role' => 'required|integer|max:11',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $bodyContent = $request->all();
        if(($bodyContent["role"] != 0 && $bodyContent["role"] != 1) || !is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        else{   
            $user = User::find($id);
            if(empty($user)){
                return response()->json(array("error"=>"Not Found"), 404);
            }
            $loggedInUser = JWTAuth::parseToken()->authenticate(); 
            $loggedInUserInfo = User::find($loggedInUser->id);
            if($loggedInUserInfo->role != 1){
                return response()->json(array("error"=>"Forbidden"), 403);
            }
            $user->role = $bodyContent["role"];
            $user->save();
            return response()->json($user, 200);
        }  
    }

    public function createUser(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:5|confirmed',
            ]);
    
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        else{
            $bodyContent = $request->all();
            $user = new User;
            $user->username = $bodyContent["username"];
            $user->email = $bodyContent["email"];
            $user->password = Hash::make($bodyContent["password"]);
            $user->role = 0;
            $user->save();
            $token = JWTAuth::fromUser($user);
    
            return response()->json(compact('user','token'),201);
        }
    }

    public function editUser($id, Request $request){
        if(!is_numeric($id)) {
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:255|nullable',
            'surname' => 'string|max:255|nullable',
            'phone_number' => 'string|regex:/(8)[0-9]{8}/|nullable',
            'description' => 'string|max:255|nullable',
            ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate();
        $user = User::find($id);
        if(empty($user)){
            return response()->json(array("error"=>"Not Found"), 404);
        }
        if($user->id == $loggedInUser->id){
            $bodyContent = $request->all();
            $user->name = $bodyContent["name"];  
            $user->surname = $bodyContent["surname"];
            $user->phone_number = $bodyContent["phone_number"];
            $user->description = $bodyContent["description"];
            $user->save();
            return response()->json($user, 200);
        }
        else {
            return response()->json(array("error"=>"Unauthorized"), 401);
        }
    }

    public function deleteUser($id){
        if(!is_numeric($id)){
            return response()->json(array("error"=>"Bad Request"), 400);
        }
        $loggedInUser = JWTAuth::parseToken()->authenticate(); 
        $loggedInUserInfo = User::find($loggedInUser->id);

        if($loggedInUserInfo->role != 1){
            return response()->json(array("error"=>"Forbidden"), 403);
        }
        else{
            $user = User::find($id);
            if($user != null){
                $userLists = ReadList::where('user_id', $id)->get();
                foreach ($userLists as $userList){
                    ReadListBook::where('read_list_id',  $userList->id)->delete();
                }
                ReadList::where('user_id', $id)->delete();
                Comment::where('user_id', $id)->delete();
                $user->delete();
                if($loggedInUser->id == $id){
                    JWTAuth::parseToken()->invalidate($loggedInUser);
                }
                return response()->json(null, 204);
            }
            else{
                return response()->json(array("error"=>"Not Found"), 404);
            }
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function logout( Request $request ) {

        $token = $request->header( 'Authorization' );

        try {
            JWTAuth::parseToken()->invalidate( $token );

            return response()->json( [
                'error'   => false,
                'message' => trans( 'auth.logged_out' )
            ] );
        } catch ( TokenExpiredException $exception ) {
            return response()->json( [
                'error'   => true,
                'message' => trans( 'auth.token.expired' )

            ], 401 );
        } catch ( TokenInvalidException $exception ) {
            return response()->json( [
                'error'   => true,
                'message' => trans( 'auth.token.invalid' )
            ], 401 );

        } catch ( JWTException $exception ) {
            return response()->json( [
                'error'   => true,
                'message' => trans( 'auth.token.missing' )
            ], 500 );
        }
    }
}
