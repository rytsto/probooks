<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadList extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function readListBooks(){
        return $this->hasMany('App\ReadListBook');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public $timestamps = false;
}
