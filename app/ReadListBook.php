<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReadListBook extends Model
{
    public $timestamps = false;
    
    public function readList(){
        return $this->belongsTo('App\ReadList');
    }
}
