<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserColumnsToNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->unsigned()->nullable()->change();
            $table->string('surname')->unsigned()->nullable()->change();
            $table->string('phone_number')->unsigned()->nullable()->change();
            $table->string('description')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('name')->unsigned()->nullable(false)->change();
            $table->integer('surname')->unsigned()->nullable(false)->change();
            $table->integer('phone_number')->unsigned()->nullable(false)->change();
            $table->integer('description')->unsigned()->nullable(false)->change();
        });
    }
}
