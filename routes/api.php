<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('books', 'BooksController@getAllBooks');
Route::get('books/{id}', 'BooksController@getBookById');
Route::get('books/{id}/comments', 'CommentsController@getAllCommentsByBook');

Route::get('users/{id}', 'UsersController@getUserById');
Route::post('login', 'UsersController@authenticate');
Route::post('users', 'UsersController@createUser');

Route::get('comments/{id}', 'CommentsController@getCommentById');

Route::get('lists', 'BooksListsController@getPublicBooksLists');
Route::get('lists/{id}/comments', 'CommentsController@getAllCommentsByList');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UsersController@getAuthenticatedUser');
    Route::post('logout', 'UsersController@logout');

    Route::post('books', 'BooksController@createBook');
    Route::post('books/{id}/comments', 'CommentsController@createBookComment');
    Route::put('books/{id}', 'BooksController@editBook');
    Route::put('books/{bookId}/comments/{commentId}', 'CommentsController@editBookComment');
    Route::delete('books/{id}', 'BooksController@deleteBook');
    Route::delete('books/{bookId}/comments/{commentId}', 'CommentsController@deleteBookComment');

    Route::get('users', 'UsersController@getAllUsers');
    Route::get('users/{id}/lists', 'BooksListsController@getBooksListsByUser');
    Route::put('users/{id}', 'UsersController@editUser');
    Route::patch('users/{id}', 'UsersController@setUserRole');
    Route::delete('users/{id}', 'UsersController@deleteUser');

    Route::get('lists/{id}', 'BooksListsController@getBooksListById');
    Route::post('lists', 'BooksListsController@createBooksList');
    Route::post('lists/{id}/add', 'BooksListsController@addBookToList');
    Route::post('lists/{id}/comments', 'CommentsController@createListComment');
    Route::put('lists/{id}', 'BooksListsController@editBooksList');
    Route::put('lists/{listId}/comments/{commentId}', 'CommentsController@editListComment');
    Route::patch('lists/{listId}/books/{bookInListId}', 'BooksListsController@setBookState');
    Route::delete('lists/{id}', 'BooksListsController@deleteBooksList');
    Route::delete('lists/{listId}/books/{bookInListId}', 'BooksListsController@removeBookFromList');
    Route::delete('lists/{listId}/comments/{commentId}', 'CommentsController@deleteListComment');
});